DELETE FROM 
    awjf_statische_waldgrenzen_oereb.transferstruktur_geometrie
;
DELETE FROM 
    awjf_statische_waldgrenzen_oereb.transferstruktur_hinweisvorschrift
;
DELETE FROM
    awjf_statische_waldgrenzen_oereb.localiseduri
;
DELETE FROM 
    awjf_statische_waldgrenzen_oereb.multilingualuri 
;
DELETE FROM 
    awjf_statische_waldgrenzen_oereb.dokumente_dokument
;
DELETE FROM 
    awjf_statische_waldgrenzen_oereb.transferstruktur_eigentumsbeschraenkung
;
DELETE FROM 
    awjf_statische_waldgrenzen_oereb.transferstruktur_legendeeintrag
;
DELETE FROM 
    awjf_statische_waldgrenzen_oereb.transferstruktur_darstellungsdienst
;



-- DELETE FROM
--     awjf_statische_waldgrenzen_oereb.vorschriften_hinweisweiteredokumente
-- WHERE
--     t_datasetname = 'ch.so.awjf.waldgrenzen'
-- ;
-- DELETE FROM 
--     awjf_statische_waldgrenzen_oereb.transferstruktur_hinweisvorschrift
-- WHERE
--     t_datasetname = 'ch.so.awjf.waldgrenzen'
-- ;
-- DELETE FROM 
--     awjf_statische_waldgrenzen_oereb.vorschriften_dokument
-- WHERE
--     t_datasetname = 'ch.so.awjf.waldgrenzen'
-- ;
-- DELETE FROM 
--     awjf_statische_waldgrenzen_oereb.transferstruktur_geometrie
-- WHERE 
--     t_datasetname = 'ch.so.awjf.waldgrenzen'
-- ;
-- DELETE FROM 
--     awjf_statische_waldgrenzen_oereb.transferstruktur_eigentumsbeschraenkung
-- WHERE
--     t_datasetname = 'ch.so.awjf.waldgrenzen'    
-- ;
-- DELETE FROM
--     awjf_statische_waldgrenzen_oereb.transferstruktur_darstellungsdienst
-- WHERE
--     t_datasetname = 'ch.so.awjf.waldgrenzen'
-- ;
